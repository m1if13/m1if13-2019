# TP 8 - Web Workers

## Objectif

  - Créer et mettre en oeuvre des Web workers dans une application Web

## Application

Vous reprendrez l'application des fourmis de Langton du TP précédent. L'idée est de faire tourner l'algorithme des fourmis dans un worker et de gérer l'affichage dans l'application elle-même.

## Préambule

Dans votre application, vous aurez d'un côté le code JS "principal", qui créera un worker et mettra à jour l'image représentant la trace d'une fourmi dans un canvas (cf. TP précédent),
et de l'autre, le code "worker" qui fera tourner indéfiniment l'algorithme d'une fourmi.
Vous fonctionnerez sur un mode événementiel : à chaque étape, le worker enverra un événement indiquant le point modifié et la couleur de modification.
Le code principal s'bonnera à ces événements et modifiera le canvas en conséquence.

Dans un premier temps, définissez le format d'échange de données entre ces deux parties du code.

**Rappel** : pas besoin d'échanger un tableau représentant la totalité de l'image ; les modifications apportées par la fourmi suffisent

## Dedicated worker

Réalisez les deux parties du code JS : le code principal appelé dans la page HTML et le code worker dans un ``Dedicated Worker``).
Exécutez l'application et vérifiez son fonctionnement.

### Analyse des performances

&Agrave; l'aide de l'outil d'analyse des performances du navigateur, identifiez dans quelle partie du code l'application passe le plus de temps. Proposez une solution d'amélioration.

## Shared worker

Faites maintenant en sorte que vous puissiez ouvrir plusieurs onglets permettant chacun de rajouter une fourmi.
Le code de Langton s'exécutera dans un ``Shared Worker`` qui gèrera un tableau de fourmis, évoluant dans un environnement commun.
&Agrave; chaque nouvelle connexion au worker (nouvel onglet), il ajoutera une fourmi au tableau.
Cependant, chaque onglet n'affichera que la trace de "sa" fourmi.

