# TP 3 - Web mobile

## Objectifs

  - Appliquer les principes du Responsive Web Design
  - Utiliser les capteurs et les actionneurs disponibles sur un terminal mobile
  - Adapter une application à un contexte mobile

 
## Application

Vous continuerez de travailler sur l'application cartographique des TPs précédents, dont la partie côté client est une SPA qui 'appuie sur le framework Vue.js et son routeur. Dans ce TP, vous allez faire en sorte qu'elle soit plus facilement utilisable sur un terminal mobile.

## Outils

Pour cela, il est nécessaire de pouvoir le tester sur une autre machine que celle sur laquelle vous développez. Le wifi ne permet pas aux différentes machines qui y sont connectées de communiquer entre elles. Il vous faudra donc utiliser une autre solution (par exemple, un partage de connexion en "tethering" wifi).

## Travail à réaliser

### Responsive Web Design

Vous allez faire en sorte que votre application soit "responsive", c'est-à-dire que l'affichage s'adapte à la taille de l'écran. Pour cela, vous pourrez utiliser :
- les technologies de base : [CSS Media queries](https://www.w3.org/TR/css3-mediaqueries/) + [CSS Grid Layout Level 1](https://www.w3.org/TR/css-grid-1/)
- la bibliothèque [Foundation](https://foundation.zurb.com/)

**Aide** : il est aussi conseillé d'utiliser des préprocesseurs CSS (comme [SASS/SCSS](https://sass-lang.com/)) pour générer une feuille de style personnalisée pour l'utilisateur.

### Touch events

Mettez en place des interactions à plusieurs doigts, pour pouvoir par exemple se déplacer zoomer ou tirer de façon différentes.

### Utilisation de capteurs : [Geolocation API](https://www.w3.org/TR/geolocation-API/)

Faites en sorte de remplacer le positionnement "manuel" de l'utilisateur par la remontée de ses coordonnées GPS si celles-ci sont disponibles.

### Utilisation d'actionneurs : [Vibration API](https://www.w3.org/TR/vibration/)

Faites en sorte que le terminal vibre pour signaler à l'utilisateur que c'est à lui de jouer.

### Adaptation au contexte mobile

Faites en sorte que lorsque l'utilisateur se met à courrir (d'après les valeurs de l'[accéléromètre](https://www.w3.org/TR/accelerometer/)), l'application affiche le temps restant avant de passer son tour.

## Rendu

  - Cette partie doit être pushée sur la forge le lundi 18 mars à 23h59.