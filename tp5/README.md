# TP 5 - Cross-Origin Request System

## Objectifs

  - Mettre en place des requêtes cross-domaine
  - concevoir un mashup de données côté client

## Application

Vous continuerez de travailler sur l'application cartographique des TPs précédents. Dans ce TP, vous allez faire en sorte qu'elle interroge un serveur différent et donc passer outre la Same Origine Policy. Concrètement, il s'agit d'utiliser un outil de géocodage (geocoding).

## Outils

Vous utiliserez celui de **[Mapbox](https://www.mapbox.com/search/)**. Mapbox est une Web API qui propose plusieurs services (cartes, géocoding, réalité virtuelle...). Vous utiliserez le service de _reverse geocoding_.

**Remarque** : vous aurez besoin d'un _access token_ pour obtenir une réponse de cette API.

- [Création d'un access token](https://mapbox.com/help/define-access-token)
- [Documentation](https://docs.mapbox.com/api/)
- [Playground](https://docs.mapbox.com/search-playground/#{%22url%22:%22%22,%22index%22:%22mapbox.places%22,%22staging%22:false,%22onCountry%22:true,%22onType%22:true,%22onProximity%22:true,%22onBBOX%22:true,%22onLimit%22:true,%22onLanguage%22:true,%22countries%22:[],%22proximity%22:%22%22,%22typeToggle%22:{%22country%22:false,%22region%22:false,%22district%22:false,%22postcode%22:false,%22locality%22:false,%22place%22:false,%22neighborhood%22:false,%22address%22:false,%22poi%22:false},%22types%22:[],%22bbox%22:%22%22,%22limit%22:%22%22,%22autocomplete%22:true,%22languages%22:[],%22languageStrict%22:false,%22onDebug%22:false,%22selectedLayer%22:%22%22,%22debugClick%22:{},%22localsearch%22:false,%22query%22:%224.865671286746533,45.78213993672148%22})

## Travail à réaliser

Dans votre boucle qui actualise la position toutes les 10 secondes, rajoutez une requête au reverse geocoder, pour pouvoir obtenir l'adresse à laquelle vous vous situez. &Agrave; la réception de la réponse de Mapbox, parsez le JSON et :

- affichez cette adresse dans un div / composant texte spécifique situé **au bas de l'écran** (aide : [Bottom navigation bar](https://www.w3schools.com/howto/howto_css_bottom_nav.asp) sur W3Schools)
- envoyez cette adresse à votre serveur en WebSocket, pour qu'il permette de vous localiser également sur l'interface administrateur

## Déploiement (rappel)

Si vous ne l'avez pas fait en TP, vous demanderez une VM à Lionel Médini par mail, en mettant en CC votre binôme. Son adresse commencera par `192.168.75.`. La fin de son adresse IP sera inscrite dans Tomuss dans la case "IP_VM_Vue".

Vous déploierez votre application à la racine de cette VM et **sur le port 80** (-> en `sudo`).

Vous y accéderez :

- depuis Eduroam : par son adresse IP
- depuis n'importe quel autre réseau (4G...) : par l'adresse https://proxy-tps-m1if13-2019.univ-lyon1.fr/XXX/ où XXX est la valeur inscrite dans Tomuss

C'est cette dernière adresse qui sera utilisée lors de la phase finale de démonstration. Par conséquent, **veillez à ce qu'aucune URL absolue ne soit utilisée dans votre code**, puisque l'URL exposée par le proxy n'est pas celle à laquelle votre serveur répondra.

## Rendu (décalé **pour cette partie**)

- Votre TP doit être pushé sur la forge le lundi 15 avril 2019 à 23h59
- Indiquez dans Tomuss l'URL de base (sans .git) de votre projet
- Vérifiez que Lionel Médini et Brahim Boukoufallah sont reporters de votre projet

Pour rappel, l'objectif est de tester vos applications le mardi 16 avril.