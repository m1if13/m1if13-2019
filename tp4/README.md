# TP 4 - WebSockets

## Objectifs

  - Utiliser un module serveur compatible WebSocket
  - Mettre en oeuvre l'API WebSocket

 
## Application

Vous continuerez de travailler sur l'application cartographique des TPs précédents, dont la partie côté client est une SPA qui a été adaptée au mobile dans le TP précédent. Dans ce TP, vous allez faire en sorte qu'elle communique en temps réel avec le serveur.

## Outils

**Côté serveur** : vous utiliserez le package [`express-ws`](https://github.com/HenningM/express-ws). Comme son nom l'indique, ce package vous permettra de continuer à utiliser votre serveur HTTP avec Express et d'y adjoindre, en parallèle, la prise en charge des WebSockets. Cet outil s'appuie sur [la bibliothèque `ws`](https://github.com/websockets/ws) qui standardise l'utilisation des WebSockets avec Node.

**Côté client** : vous utiliserez l'[API WebSocket](http://www.w3.org/TR/websockets/).

## Exemple de code

Dans le répertoire de ce TP, vous trouverez une petite application simple mêlant requêtes HTTP et WebSockets. Cette application est déployée sur une VM de l'infra et accessible à l'URL https://proxy-tps-m1if13-2019.univ-lyon1.fr/30/ (voir section Déploiement).

## Travail à réaliser

### Mise en place des WebSockets

Commencez par utiliser la [doc d'`express-ws`](https://www.npmjs.com/package/express-ws) pour installer ce module dans votre projet. Pour le côté client, vous n'avez rien à installer : les versions récentes de la plupart des navigateurs supportent le [protocole](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API#Browser_compatibility) et l'[API](https://developer.mozilla.org/en-US/docs/Web/API/WebSocket#Browser_compatibility) WebSocket.

**Remarque** : la spécification indique qu'une WebSocket n'est accessible pour un client que depuis localhost ou un serveur en HTTPS. Nous avons mis en place un proxy HTTPS pour vous permettre de tester sur l'infra de cloud du département (voir section Déploiement).

### Modification de l'application

Vous allez reprendre les modalités de communication entre le client et le serveur :

- Conservez des échanges en HTTP pour les processus de connexion, de déconnexion et de tir
- Passez en WebSocket pour les échanges suivants :
  - Envoi de la position du client au serveur toutes les 10 secondes.
  - Gestion des "tours" : supprimez le timeout côté client (qui permet d'envoyer une requête pour savoir si c'est à l'utilisateur de jouer) et faites en sorte que le serveur informe le client quand son tour commence et quand il finit (rappel : un tour commence quand l'utilisateur adverse a tiré et après l'envoi d'une réponse "négative" par le serveur)

**Aide** : attention, les WebSockets ne transmettent pas de headers et il n'y a donc pas de gestion de sessions entre le serveur et le client. &Agrave; vous de faire en sorte que le serveur sache à quel joueur il répond lorsqu'il traite un message. Le plus simple à mon avis est de mémoriser à quel joueur correspond chaque socket au moment de la connexion (voir [doc de `ws`](https://github.com/websockets/ws#simple-server)).

### Finalisation de l'application

Ajoutez un moyen pour le serveur d'informer le client :

- qu'une partie vient de commencer,
- que le joueur adverse a manqué son tir,
- qu'il a réussi son tir et que la partie est terminée,
- qu'il s'est déconnecté ou que sa position n'est plus actualisée depuis 40 secondes.

Ajoutez une interface "admin" (pour les encadrants de TP) qui contiendra :

- les logs du serveur indiquant (comme une console) qui se connecte, les positions reçues, les tirs, les résultats...
- un formulaire permettant d'envoyer une requête HTTP contenant un message au serveur ; ce message sera transmis par WebSocket à tous les joueurs (par exemple pour leur demander de revenir au Nautibus).

Cette interface d'administration devra être accessible à l'URL `admin` à partir de la racine de votre application.

## Déploiement

Si vous ne l'avez pas fait en TP, vous demanderez une VM à Lionel Médini par mail, en mettant en CC votre binôme. Son adresse commencera par `192.168.75.`. La fin de son adresse IP sera inscrite dans Tomuss dans la case "IP_VM_Vue".

Vous déploierez votre application à la racine de cette VM et **sur le port 80** (-> en `sudo`).

Vous y accéderez :

- depuis Eduroam : par son adresse IP
- depuis n'importe quel autre réseau (4G...) : par l'adresse https://proxy-tps-m1if13-2019.univ-lyon1.fr/XXX/ où XXX est la valeur inscrite dans Tomuss

C'est cette dernière adresse qui sera utilisée lors de la phase finale de démonstration. Par conséquent, **veillez à ce qu'aucune URL absolue ne soit utilisée dans votre code**, puisque l'URL exposée par le proxy n'est pas celle à laquelle votre serveur répondra.

## Rendu

- Votre TP doit être pushé sur la forge le lundi 8 avril 2019 à 23h59
- Indiquez dans Tomuss l'URL de base (sans .git) de votre projet
- Vérifiez que Lionel Médini et Brahim Boukoufallah sont reporters de votre projet

Pour rappel, l'objectif est de tester vos applications le mardi 19 entre midi et deux (avant le début du CM).