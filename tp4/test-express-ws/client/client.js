// On récupère l'URL de base du serveur (pour pouvoir tester avec ou sans proxy)
const serverURL = window.location.hostname + (window.location.port === '' ? '' : ':' + window.location.port) + window.location.pathname.replace('index.html', '');
console.log('Serveur :', serverURL);

// PARTIE WS
// https://developer.mozilla.org/en-US/docs/Web/API/WebSocket

// Create WebSocket connection.
const socket = new WebSocket('wss:' + serverURL);

// Connection opened
socket.addEventListener('open', () => {
    affiche('Socket ouverte.', 'Gestion WS');
});

// Listen for messages
socket.addEventListener('message', event => {
  affiche(event.data, 'WS');
});

function sendWS() {
  socket.send(document.getElementById('message').value);
}

function closeWS() {
  // https://developer.mozilla.org/en-US/docs/Web/API/WebSocket/close
  socket.close(1000, 'J\'en ai marre...');
  // socket.close();
  affiche('Socket fermée.', 'Gestion WS');
}

// PARTIE HTTP
// https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch

function sendHTTP() {
  fetch('test/' + document.getElementById('message').value)
  .then(response => response.text())
  .then(text => { affiche(text, 'HTTP'); });
}

// UTILITAIRE

function affiche(message, source) {
  document.getElementById('res').innerHTML += '<strong>' + source + '</strong> : ' + message + '<br>';
}
