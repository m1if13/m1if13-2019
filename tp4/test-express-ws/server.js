// Exemple d'utilisation du module express-ws
// Permet d'utiliser le même serveur en HTTP et en WS en même temps

const express = require('express');
const app = express();
const  session = require('express-session');
const expressWs = require('express-ws')(app);

let users = 0;

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true
}));

// PARTIE HTTP
// https://expressjs.com/en/4x/api.html (rappel)

app.get('/test/:message', function(req, res, next){
	if(!req.session.number) {
		req.session.number = users;
		users++;
	}
    console.log('[User' + req.session.number + ']: Get :', req.params.message);
    res.send('Espèce de ' + req.params.message + ' !');
  });

app.get('/*', express.static('client'));

// Partie WebSocket
// https://github.com/HenningM/express-ws
// https://github.com/websockets/ws (module utilisé par express-ws)

app.ws('/', function(ws, req) {
  ws.on('message', function(msg) {
    console.log('[User' + req.session.number + ']: Socket : ', msg);
    setTimeout(() => {
      ws.send(msg + ' toi-même !');
    }, 2000)
  });
  ws.on('close', function(code, msg) {
    console.log('Fermeture socket : ' + req.connection.remoteAddress, 'code : ' + code + ', message : ' + msg);
  });
  console.log('Connexion socket :', req.connection.remoteAddress);
});

// ...

const port = 80;
app.listen(port);
console.log('Server listening on port', port);
