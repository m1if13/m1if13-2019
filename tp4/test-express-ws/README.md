# Test Express-WS

**Petite appli d'aide à l'utilisation des WebSockets avec express-ws**

Cette application met en oeuvre à la fois des requêtes-réponses HTTP et des échanges de messages en WebSocket.

## Installation

```javascript
npm install
```

## Exécution

```javascript
node server.js
```

Les pointeurs vers la documentation sont dans les commentaires du code.