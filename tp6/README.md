# TP 6 - Progressive Web Applications

## Objectif

  - Transformer une application Web existante en PWA

## Application

Vous continuerez de travailler sur l'application cartographique des TPs précédents. Dans ce TP, vous allez faire en sorte qu'elle soit installable sur un ordinateur ou mobile et qu'elle démarre plus rapidement.

## Outils

Vous pouvez utiliser le plugin [LightHouse](https://developers.google.com/web/tools/lighthouse/) de Chrome.

## Préambule

Dans un premier temps, réfléchissez aux ressources qui sont cachables et à celles qui ne le sont pas. Si vous avez mis en place un app shell (ce qui devrait être le cas pour une SPA), les composants de cet app shell sont déjà dans la première catégorie. En première approximation, vous pouvez déjà faire une PWA qui cache ces éléments.

## Déploiement (rappel)

Les PWA ne fonctionnent qu'en HTTPS. Vous devrez donc utiliser votre VM pour tester le fonctionnement de la vôtre.

## Travail à réaliser

### Première PWA

Vous allez suivre les différentes étapes du [tutoriel MDN](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps) sur les PWA :

- créez le fichier manifest qui permet de déclarer votre application
- ajoutez un logo qui sera l'icône de déploiement de votre application
- modifiez le fichier index.html en conséquence
- à l'aide du fichier manuifest, rendez votre application installable
- créez le ``service worker`` qui permettra de cacher les ressources choisies

### Amélioration (bonus)

Modifiez votre service worker pour qu'il cache également les données applicatives telles que :

- le profil de l'utilisateur
- les données de cartes téléchargées
- les données obtenues d'autres sources de données (TP5)

Mesurez l'amélioration des performances entre l'application en fin de TP5 et celle-ci.

## Rendu (décalé **pour cette partie**)

- Votre TP doit être pushé sur la forge le lundi 15 avril 2019 à 23h59
- Indiquez dans Tomuss l'URL de base (sans .git) de votre projet
- Vérifiez que Lionel Médini et Brahim Boukoufallah sont reporters de votre projet

Pour rappel, l'objectif est de tester vos applications le mardi 16 avril.