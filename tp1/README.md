# TP 1 - prise en main de la stack JS

## Objectifs

Comprendre le rôle et utiliser les principaux outils de gestion de projet en JS

  - NPM : gestion de dépendances côté serveur
  - Webpack : packaging du code, exécution de tâches
  - Node : exécution côté serveur
  - Express : serveur Web

## Pointeurs

Documentation et tutos :

  - NPM : [créer un projet](https://docs.npmjs.com/creating-a-package-json-file)
  - Express :
    - [lancer un serveur](https://expressjs.com/en/starter/hello-world.html)
	- [servir des fichiers statiques](https://expressjs.com/en/starter/static-files.html)
	- [Routage (répondre à des URLs spécifiques)](https://expressjs.com/en/guide/routing.html)
	- [Traiter des données de formulaire](https://www.npmjs.com/package/body-parser)
 - Webpack :
    - [installation](https://webpack.js.org/guides/installation/)
	- [packager un projet](https://webpack.js.org/guides/getting-started/)
	- [créer et gérer des `assets`](https://webpack.js.org/guides/asset-management/)
  - Leaflet (OpenStreetMap) : [utiliser la carte](https://leafletjs.com/examples/quick-start/)
 
## Application

Dans ce TP et les suivants, vous allez travailler sur une application cartographique simple, que vous améliorerez par la suite. Une version de base de cette application vous est fournie dans ce dépôt et dans le répertoire `tp1`. Ouvrez le fichier index.htmll dans votre navigateur et vérifiez que tout fonctionne bien : toute modification ou validation du formulaire doit provoquer un déplacement / changement de zoom sur la carte. Examinez le code pour le comprendre et surtout pour connaître les dépendances.

## Travail à réaliser

### Initialisation

  - Créez un dépôt sur la forge dans lequel vous stockerez votre travail. Ajoutez-y votre binôme et vos deux encadrants : Lionel Médini & Brahim Boukoufallah
  - Dans ce dépôt, créez un projet NPM à l'aide du tuto mentionné plus haut. Attention : utilisez la commande `npm init` sans l'option `--yes` pour pouvoir entrer autre chose que les valeurs par défaut.
  - Si npm a créé le `.gitignore`, vérifiez ce fichier et complétez-le au besoin. Sinon, crééez-le et remplissez-le vous-mêmes.
  - Intégrez les dépendances de la page principale au projet en les cherchant dans npm et en les installant **localement**. Remplacez les requêtes aux CDNs (ce n'est pas une bonne pratique, mais c'est pour l'exercice) par des requêtes dans le repository local (`node_modules`).
  - **Avant de pusher sur la forge, vérifiez bien que le répertoire node_module n'est pas inclus dans votre `git status`.**

### Serveur Web

  - Utilisez les tutos sur Node / Express pour démarrer un serveur Web (en mode Hello World).
  - Dans votre projet, rajoutez un répertoire `public` (ou `src`) et faites en sorte que votre serveur réponde aux requêtes en servant directement les fichiers situés dans ce répertoire.

### Packaging

  - Ajoutez Webpack aux dépendances de dévelpopement du projet, comme indiqué dans le tutoriel d'installation.
  - Générez le bundle correspondant à votre projet et créez un fichier de configuration Webpack pour le définir en tant que module.

Aide : pour donner accès facilement à jQuery (qui est utilisé dans `form.js` et `map.js`), vous pouvez utiliser `ProvidePlugin` : https://stackoverflow.com/questions/28969861/managing-jquery-plugin-dependency-in-webpack

  - rajoutez une commande `npm run build` dans le fichier package.json pour pouvoir lancer le build facilement. Vous pouvez rajouter d'autres commandes qui simplifient le déploiement (watch...), mais dans ce cas, indiquez dans votre readme quelle commande doit être exécutée pour lancer votre applcation.
  - Utilisez le tutoriel sur les assets de Webpack pour produire plusieurs modules (en incluant aussi les CSS) qui dépendront les uns des autres.

Aide :
  - Pour que jQuery soit disponible nativement dans toute l'application, vous pouvez utiliser (`webpack.ProvidePlugin`)[https://webpack.js.org/plugins/provide-plugin/]
  - Il y a une issue non résolue dans Leaflet  : https://github.com/Leaflet/Leaflet/issues/6496 qui provoque le problème suivant : une fois les icônes fournis avec Leaflet packagés avec webpack, ils ne sont pas retrouvés par la lib à cause d’une erreur de regexp. Le workaround est de rajouter dans map.js (uniquement dans la version webpack) une ligne comme :
  ```javascript
  L.Icon.Default.imagePath` = '/lib/leaflet/dist/images/';
  ```

où la partie droite permet d'accéder par une requête HTTP au répertoire contenant les icônes Leaflet.

### Modification de l'application

Vous allez maintenant étendre votre application, en vous aidant des documentations des différents outils. Ces extensions devront être faites dans un ou plusieurs modules supplémentaires, déployés comme précédemment.

  - Rajoutez au formulaire un bouton "Nouvelle partie". Ce bouton permettra au serveur de choisir un point au hasard sur la carte (proche du Nautibus) et de le mémoriser pour votre session.
  - Dans la fonction de validation du formulaire, rajoutez une requête AJAX en POST qui envoie au serveur les coordonnées. Faites en sorte que le serveur stocke chaque envoi de coordonnées dans un tableau, et renvoie :
    - Si la distance est supérieure à 10 mètres, une réponse contenant un message indiquant que le coup est manqué, et la distance séparant ces coordonnées du point choisi par le serveur ; côté client, vous  afficherez un cercle rouge (ou une animation de votre choix) à l'endroit correspondant sur la carte.
    - Si la distance est inférieure à 10 mètres, une réponse contenant un message indiquant que le coup est réussi, ainsi que les coordonnées exactes du point choisi et le nombre d'essais avant d'avoir atteint la cible ; affichez ce point sur la carte avec un message de félicitations.

## Rendu

  - Cette partie doit être pushée sur la forge le lundi 18 février à 23h59.