Initiation à WebAssembly
========================

auteur

:   [Pierre-Antoine Champin](http://champin.net/)

formation

:   [M1 Informatique](http://master-info.univ-lyon1.fr/M1/) -
    [UCBL1](https://www.univ-lyon1.fr/)

licence

:   ![cc](https://licensebuttons.net/l/by-nc-nd/4.0/88x31.png)

url

:   <http://champin.net/2019/wasm/>

Liens utiles
------------

-   [Site de référence WebAssembly.org](https://webassembly.org/)
-   [API Javascript sur
    MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WebAssembly)
-   [IDE en ligne WebAssembly.studio](http://webassembly.studio/)
-   [Exemple commenté n°1 : chargement du code
    WASM](https://webassembly.studio/?f=lhuvec8od5c)
-   [Exemple commenté n°2 : communication entre WASM et
    JS](https://webassembly.studio/?f=hxhgq527ja)
-   [Exemple commenté n°3 : images en
    WASM](https://webassembly.studio/?f=ayvlyfa6xhe)
-   [Les tableaux typés en
    Javascript](https://devdocs.io/javascript/global_objects/typedarray)

Sujet de TP
-----------

L\'objectif est d\'afficher, dans un canvas, l\'évolution d\'une [fourmi
de Langton](https://fr.wikipedia.org/wiki/Fourmi_de_Langton).

L\'environnement de la fourmi sera représenté par une image, dont chaque
pixel représente une \"case\" blanche ou noire (la fourmi elle même ne
sera pas représentée dans l\'image).

Le module WASM fournira :

-   un tableau d\'entiers représentant l\'image (cf. exemple n°3
    ci-dessus) ;
-   une fonction pour initialiser le tableau ;
-   une fonction pour simuler un mouvement de la fourmi et mettre à jour
    le tableau.

Le code JS assurera se chargera de l\'affichage du tableau dans un
canvas, et de contrôler le lancement et l\'arrêt de la simulation.
